// MedicalBlackBoxRecorder.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>
#include <string>
#include <fstream>
#include <thread>
#include <mutex>

using namespace std;

const int MAX_THREADS = 3;
const string NON_VOLATILE_STORAGE_FILENAME = "BlackBoxStorage.csv";
const string TEMPERATURE = "temperature";
const string PRESSURE = "pressure";
const string HUMIDITY = "humidity";
const string delimiter = ",";

mutex mtx;

int read_data(string metric) {
	ifstream record_file;
	string read_line;
	string saved_metric;

	record_file.open(NON_VOLATILE_STORAGE_FILENAME);

	if (record_file.is_open()) {

		cout << "'metric','data','timestamp" << endl;

		// The data stored is stored in the format metric,data,timstamp
		while (getline(record_file, read_line)) {
			// get the metric from the read line
			saved_metric = read_line.substr(0, read_line.find(delimiter));
			
			// metric is not defined as a filter
			if (metric == "" || metric == saved_metric) {
				cout << read_line << endl;
			}			
		}
	}

	record_file.close();

	return 0;
}

int process_input_data(string filename, string metric) {
	bool is_first_line = true;
	ifstream input_file;
	ofstream output_file;
	string input_line;
	size_t pos = 0;
	string data;
	string timestamp;
	string output_line[2];
	int num_of_tokens = 0;

	// Open the file for reading the input
	input_file.open(filename);

	// Open the non volatile file for writing
	// first check if another thread hasn't already opened the file
	if (!output_file.is_open()) {
		// The first time we open the output file we open it in append mode to ensure data is placed at the end of the file in a new line
		output_file.open(NON_VOLATILE_STORAGE_FILENAME, ios::app);
	}
	
	// Read the data line by line from the input data and save it to the non volatile storage
	if (input_file.is_open()) {
		
		if (output_file.is_open()) {
			mtx.lock();
			// read through each of the lines untill the end of file is reached
			while (getline(input_file, input_line)) {
				// We assume that the csv files only contain data and no headings
				// Data coming in has the data presented as data, timestamp
				pos = input_line.find(delimiter);
				// extract the substring which is the value we want to store
				data = input_line.substr(0, pos);
				timestamp = input_line.substr(pos + delimiter.length(), input_line.length());

				// print the data to the non volatile storage file as metric,data value,timestamp
				output_file << metric + delimiter + data + delimiter + timestamp << endl;
			}
			mtx.unlock();
		}
		else {
			cout << "Could not open output file " + NON_VOLATILE_STORAGE_FILENAME + " for non volatile storage." << endl;
		}
		
		input_file.close();

		if (output_file.is_open()) {
			output_file.close();
		}

		return 0;
	}
	else {
		cout << "Could not open input file " + filename << endl;
		return 0;
	}
}

static void show_usage()
{
	cerr << "Medical Black Box Recorder\n"
		 << "Usage: MedicalBlackBox.exe [Options]\n"
		 << "Options: --help  -h                    Shows this message\n"
		 << "         --temperature <filename.csv>  Record the temperature data given in the file provided. Can be combined with other metrics.\n"
		 << "         --pressure <filename.csv>     Record the pressure data given in the file provided. Can be combined with other metrics.\n"
		 << "         --humidity <filename.csv>     Record the humidity data given in the file provided.  Can be combined with other metrics.\n"
		 << "         --read  -r  <metric>          Read the data back from the non volatile storage.\n"
		 << "\n"
		 << "Example: MedicalBlackBox.exe --temperature temp.csv --pressure pressure.csv --humidity humidity.csv"
		 << endl;
}

int main(int argc, char* argv[])
{
	string input_file;
	string metric = "";
	thread threads[MAX_THREADS];
	int t_count = 0;

	// First we need to retrieve the arguments from the command line
	// error check the arguments
	if (argc == 1 || argc > 7) {
		// No inputs have been defined or too many inputs have been defined
		show_usage();
	}
	else {
		// Loop through the arguments and extract the file names or instantiate the read back
		for (int i = 1; i < argc; i++) {
			string arg = argv[i];

			if (arg == "--help" || arg == "-h") {
				show_usage();
			}
			else if (arg == "--temperature") {
				// Next argument should be the temperature filename and we should do some checks to ensure it is the rght format
				input_file = argv[i + 1];
				// Increment the iterator by one as we do not need to check the next argument for special case
				i++;

				// pass filename to data processor
				threads[t_count] = thread(process_input_data, input_file, TEMPERATURE);
				t_count++;
			}
			else if (arg == "--pressure") {
				// Next argument should be the pressure filename and we should do some checks to ensure it is the rght format
				input_file = argv[i + 1];
				// Increment the iterator by one as we do not need to check the next argument for special case
				i++;

				// pass filename to data processor
				threads[t_count] = thread(process_input_data, input_file, PRESSURE);
				t_count++;
			}
			else if (arg == "--humidity") {
				// Next argument should be the humidity filename and we should do some checks to ensure it is the rght format
				input_file = argv[i + 1];
				// Increment the iterator by one as we do not need to check the next argument for special case
				i++;

				// pass filename to data processor
				threads[t_count] = thread(process_input_data, input_file, HUMIDITY);
				t_count++;
			}
			else if (arg == "--read" || arg == "-r") {
				// there are more arguments passed in after the read option. We assume only the single metric we would like to read
				if (i != (argc - 1)) {
					metric = argv[i + 1];
				}
				
				return read_data(metric);
			}
			else {
				show_usage();
			}
		}

		if (t_count > 0) {
			cout << "Run threads" << endl;
			for (int t = 0; t < t_count; t++) {
				threads[t].join();
			}
			cout << "Threads ran successfully" << endl;
		}
	}	   	
}
